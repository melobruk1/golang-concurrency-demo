## Golang - API Mashup with Goroutines and Channels

## Install Golang - Linux

```bash
sudo tar -C /usr/local -xzf go1.13.5.linux-amd64.tar.gz

vim ~/.profile

(append :/usr/local/go/bin to PATH)
```

## Build

```bash
go build
```

## Test

```bash
go test -run NameOfTest
```

## Run

```bash
go run .
```

```bash
go run *.go
```
